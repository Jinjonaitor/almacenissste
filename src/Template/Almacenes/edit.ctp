<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-8">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"almacenes","action"=>"edit",$almacen->id])?>">
        <fieldset>
          <legend>Editar Almacen</legend>
          <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
              <input type="text" name="nombre" required="required" maxlength="255" id="nombre" class="form-control" placeholder="Nombre" value="<?=$almacen->nombre?>">
            </div>
          </div>
            <div class="col-lg-10 col-lg-offset-2">
              <button type="reset" class="btn btn-default">Cancelar</button>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
