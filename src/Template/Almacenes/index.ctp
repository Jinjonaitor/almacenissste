<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-8">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4>Almacenes</h4>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="formatos">
              <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach ($almacenes as $almacen): ?>
                  <tr>
                    <td class="col-md-4"><?= h($almacen->nombre) ?></td>
                    <td class="actions col-md-4">
                      <div class="btn-group btn-group-justified">
                        <a href="<?= $this->Url->build(["controller"=>"almacenes","action"=>"view", $almacen->id])?>" class="btn btn-default">Ver</a>
                          <a href="<?= $this->Url->build(["controller"=>"almacenes","action"=>"edit", $almacen->id])?>" class="btn btn-primary">Editar</a>
                          <form action="<?= $this->Url->build(["controller"=>"almacenes","action"=>"delete", $almacen->id])?>" method="post" style="display: none" name="form_eliminar<?=$almacen->id?>">
                          </form>
                          <a href="#" onclick="if (confirm(&quot;¿Seguro que quieres eliminar este almacén?&quot;)) { document.form_eliminar<?=$almacen->id?>.submit(); } event.returnValue = false; return false;"class="btn btn-danger">Eliminar</a>

                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
          </table>
          <!-- /.table-responsive -->

      </div>
      <!-- /.panel-body -->
  </div>
  <!-- /.panel -->
</div>

<!-- Blog Sidebar Widgets Column -->
<div class="col-md-4">

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Acciones:</h4>
        <div class="row">

            <!-- /.col-lg-12 -->

            <div class="col-lg-12">
                <a href="<?= $this->Url->build(["controller"=>"almacenes","action"=>"add"])?>" class="btn btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i>Agregar Almacen</a>
                <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"index"])?>" class="btn btn-primary btn-block">Ver los Productos</a>
                <a href="<?= $this->Url->build(["controller"=>"partidas","action"=>"index"])?>" class="btn btn-primary btn-block">Ver las Partidas</a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well
    <div class="well">
        <h4>Información</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>-->

</div>

<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
      $('#formatos').DataTable({
        "dom": '<"col-sm-12 pull-left"fl>rt<"bottom"ip><"clear">'
      });
    } );
  </script>
<?php $this->end();?>
