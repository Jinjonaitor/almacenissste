<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-8">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"almacenes","action"=>"add"])?>">
        <fieldset>
          <legend>Nuevo Almacen</legend>
          <div class="form-group">
            <label for="username" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
              <input type="text" name="nombre" required="required" maxlength="255" id="username" class="form-control" placeholder="Nombre">
            </div>
          </div>
            <div class="col-lg-10 col-lg-offset-2">
              <a href="<?=$this->Url->build(["controller"=>"almacenes","action"=>"index"])?>" class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
