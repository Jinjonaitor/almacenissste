<!DOCTYPE html>
<html>
  <head>

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>ISSSTE- Kardex de Almacen</title>

      <!-- Bootstrap Core CSS -->
      <?= $this->Html->css('bootstrap.min') ?>

      <!-- DataTable CSS -->
      <?= $this->Html->css('jquery.dataTables') ?>
      <?= $this->Html->css('dataTables.bootstrap.min') ?>

      <!-- Font-awesome CSS -->
      <?= $this->Html->css('font-awesome.min') ?>

      <!-- Custom CSS -->
      <?= $this->Html->css('custom') ?>
      <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
      <?= $this->Html->script(['jquery-1.11.0.min','bootstrap.min','jquery.dataTables.min', 'dataTables.bootstrap.min'])?>
      <?=$this->fetch('libraries')?>
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-top" role="navigation">
        <div class="container nav-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"index"])?>">Inicio</a>
                    </li>
                    <?php if ($this->request->session()->read('Auth.User.role')==='administrador'):?>
                      <li>
                          <a href="<?= $this->Url->build(["controller"=>"users","action"=>"index"])?>">Usuarios</a>
                      </li>
                    <?php endif;?>
                    <li>
                        <a href="<?= $this->Url->build(["controller"=>"almacenes","action"=>"index"])?>">Almacenes</a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(["controller"=>"partidas","action"=>"index"])?>">Partidas</a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"index"])?>">Productos</a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(["controller"=>"movimientos","action"=>"findByFolio"])?>">Folios</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                  <li><a href="<?= $this->Url->build(["controller"=>"users","action"=>"logout"])?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar Sesión</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <?= $this->Flash->render() ?>

    <!--contenido-->
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <a class="navbar-brand" href="#"><?=$this->Html->image('logo.png', ['alt' => 'Logo']);?></a>
          <h2 class="text-muted">Issste Hospital Regional <br>Presidente Juárez- Kardex de Almacen</h2>
        </div>
        <?= $this->fetch('content') ?>

      </div>
    </div>
    <!--/contenido-->


    <?= $this->fetch('scripts')?>
    <!-- *Arquitectura de aplicacion: Juan Jose Gonzalez Osorio
    *Diseño de Interfaz: Maria Jose Valencia Lopez -->
  </body>
</html>
