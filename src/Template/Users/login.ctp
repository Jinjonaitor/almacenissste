<?php $this->layout = false;?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Almacen ISSSTE - login</title>

    <!-- CSS -->
    <?= $this->Html->css(['bootstrap.min','custom','font-awesome.min']) ?>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-top" role="navigation">
        <div class="container nav-container">

        </div>
        <!-- /.container -->
    </nav>
    <?= $this->Flash->render() ?>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <!--Blog Entries Column -->
            <div class="col-md-5 col-md-offset-3">
              <div class="panel panel-default panel-login">
                  <div class="panel-heading text-center">
                    <?=$this->Html->image('logo.png', ['alt' => 'Logo']);?>
                    <h4 class="text-muted">Sistema de Almacen</h4>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12">
                        <form action="<?= $this->Url->build(["controller"=>"users","action"=>"login"])?>" method="post">
                          <fieldset>
                            <legend>Acceso</legend>
                            <div class="form-group">
                              <input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
                            </div>
                            <div class="form-group">
                              <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                            </div>
                            <div class="col-md-10 col-md-offset-1 boton">
                              <button type="submit" class="btn btn-primary btn-lg btn-block" name="action">Entrar</button>
                            </div>
                            <div class="form-group">

                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div>

                  </div>
                  <!-- /.panel-body -->
              </div>
              <!-- /.panel -->
            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="pull-right">Copyright &copy; Firma 2017</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
