<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-lg-8">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Detalles de Usuario</h3>
    </div>
    <div class="panel-body">
      <p><b>Apellidos:</b> <?=$user->apellidos?></p>
      <p><b>Nombres:</b> <?=$user->nombres?></p>
      <p><b>Usuario:</b> <?=$user->username?></p>
      <p><b>Rol:</b> <?=$user->role?></p>
    </div>
  </div>


</div>

<div class="col-lg-4">

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Acciones:</h4>
        <div class="row">

            <!-- /.col-lg-12 -->

            <div class="col-lg-12">
                <a href="<?= $this->Url->build(["controller"=>"users","action"=>"index"])?>" class="btn btn-primary btn-block"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Regresar</a>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well
    <div class="well">
        <h4>Información</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>-->

</div>
