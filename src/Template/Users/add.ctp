<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-8">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"users","action"=>"add"])?>">
        <fieldset>
          <legend>Nuevo Usuario</legend>
          <div class="form-group">
            <label for="username" class="col-lg-2 control-label">Usuario</label>
            <div class="col-lg-10">
              <input type="text" name="username" required="required" maxlength="255" id="username" class="form-control" placeholder="Usuario">
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-lg-2 control-label">Contraseña</label>
            <div class="col-lg-10">
              <input type="password" name="password" required="required" maxlength="255" id="password" class="form-control" placeholder="Contraseña">
            </div>
          </div>
          <div class="form-group">
            <label for="role-id" class="col-lg-2 control-label">Role</label>
            <div class="col-lg-10">
              <select class="form-control" name="role">
                  <option value="administrador">Administrador</option>
                  <option value="usuario">Usuario</option>
              </select>
              <!--<input type="number" name="categorias_id" required="required" id="categoria-id" class="form-control" placeholder="nombre del archivo">-->
            </div>
          </div>
          <div class="form-group">
            <label for="nombres" class="col-lg-2 control-label">Nombres</label>
            <div class="col-lg-10">
              <input type="text" name="nombres" required="required" maxlength="255" id="nombres" class="form-control" placeholder="Nombres">
            </div>
          </div>
          <div class="form-group">
            <label for="apellidos" class="col-lg-2 control-label">Apellidos</label>
            <div class="col-lg-10">
              <input type="text" name="apellidos" required="required" maxlength="255" id="apellidos" class="form-control" placeholder="Apellidos">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <a href="<?=$this->Url->build(["controller"=>"users","action"=>"index"])?>" class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
