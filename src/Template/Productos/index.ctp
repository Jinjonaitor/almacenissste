<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-9">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4>Productos disponibles</h4>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="productos">
              <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Clave</th>
                    <th>Presentación</th>
                    <th>Partida</th>
                    <th>Almacen</th>
                    <th>Acciones</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach ($productos as $producto): ?>
                  <tr>
                    <td class="col-md-4"><?=$producto->nombre?></td>
                    <td class="col-md-1"><?= h($producto->clave) ?></td>
                    <td class="col-md-1"><?= h($producto->presentacion) ?></td>
                    <td class="col-md-1"><?= $producto->has('partida') ? $this->Html->link($producto->partida->nombre, ['controller' => 'partidas', 'action' => 'view', $producto->partida->id]) : '' ?></td>
                    <td class="col-md-2"><?= $producto->has('almacene') ? $this->Html->link($producto->almacene->nombre, ['controller' => 'Almacenes', 'action' => 'view', $producto->almacene->id]) : '' ?></td>
                    <td class="col-md-3">
                      <div class="btn-group btn-group-sm btn-group-justified">
                        <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"view", $producto->id])?>" class="btn btn-default">Ver</a>
                        <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"edit", $producto->id])?>" class="btn btn-primary">Editar</a>
                          <form action="<?= $this->Url->build(["controller"=>"productos","action"=>"delete", $producto->id])?>" method="post" style="display: none" name="form_eliminar<?=$producto->id?>">
                          </form>
                        <a href="#" onclick="if (confirm(&quot;¿Seguro que quieres eliminar este producto?&quot;)) { document.form_eliminar<?=$producto->id?>.submit(); } event.returnValue = false; return false;"class="btn btn-danger">Eliminar</a>

                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
          </table>
          <!-- /.table-responsive -->

      </div>
      <!-- /.panel-body -->
  </div>
  <!-- /.panel -->
</div>

<!-- Blog Sidebar Widgets Column -->
<div class="col-md-3">

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Acciones:</h4>
        <div class="row">

            <!-- /.col-lg-12 -->

            <div class="col-lg-12">

                <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"add"])?>" class="btn btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i> Agregar Producto</a>
                <a href="<?= $this->Url->build(["controller"=>"almacenes","action"=>"index"])?>" class="btn btn-primary btn-block">Ver los Almacenes</a>
                <a href="<?= $this->Url->build(["controller"=>"partidas","action"=>"index"])?>" class="btn btn-primary btn-block">Ver las Partidas</a>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well
    <div class="well">
        <h4>Información</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>-->

</div>

<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
      $('#productos').DataTable({
        "dom": '<"col-sm-12 pull-left"fl>rt<"bottom"ip><"clear">'
      });
    } );
  </script>
<?php $this->end();?>
