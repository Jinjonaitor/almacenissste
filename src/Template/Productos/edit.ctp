<?php
/**
  * @var \App\View\AppView $this
  */
?><div class="col-md-8">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"productos","action"=>"edit",$producto->id])?>" enctype="multipart/form-data">
        <fieldset>
          <legend>Modificar Producto</legend>
          <div class="form-group">
            <label for="nombre" class="col-lg-2 control-label">Nombre</label>
            <div class="col-lg-10">
              <input type="text" name="nombre" required="required" maxlength="255" id="nombre" class="form-control" value="<?=$producto->nombre?>">
            </div>
          </div>
          <div class="form-group">
            <label for="clave" class="col-lg-2 control-label">Clave</label>
            <div class="col-lg-10">
              <input type="text" name="clave" required="required" maxlength="255" id="clave" class="form-control" value="<?=$producto->clave?>">
            </div>
          </div>
          <div class="form-group">
            <label for="presentacion" class="col-lg-2 control-label">Presentación</label>
            <div class="col-lg-10">
              <input type="text" name="presentacion" required="required" maxlength="255" id="presentacion" class="form-control" value="<?=$producto->presentacion?>">
            </div>
          </div>
          <div class="form-group">
            <label for="partidas-id" class="col-lg-2 control-label">Partida</label>
            <div class="col-lg-10">
              <select class="form-control" name="partidas_id">
                <?php foreach ($partidas as $partida):?>
                  <option value="<?=$partida->id?>" <?php if($partida->id === $producto->partidas_id){ echo'selected="true"';}?>><?=$partida->nombre?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="almacenes-id" class="col-lg-2 control-label">Almacen</label>
            <div class="col-lg-10">
              <select class="form-control" name="almacenes_id">
                <?php foreach ($almacenes as $almacen):?>
                  <option value="<?=$almacen->id?>" <?php if($almacen->id === $producto->almacenes_id){ echo'selected="true"';}?>><?=$almacen->nombre?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <a href="<?=$this->Url->build(["controller"=>"productos","action"=>"index"])?>" class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
