<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div >
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Descripción del Producto</h3>
      </div>
      <div class="panel-body">
        <p><b>Nombre:</b> <?=$producto->nombre?></p>
        <p><b>Clave:</b> <?=$producto->clave?></p>
        <p><b>Presentación:</b> <?=$producto->presentacion?></p>
        <p><b>Partida:</b> <a href="<?= $this->Url->build(["controller"=>"partidas","action"=>"view", $producto->partida->id])?>"> <?=$producto->partida->nombre?></a></p>
        <p><b>Almacen:</b> <a href="<?= $this->Url->build(["controller"=>"almacenes","action"=>"view", $producto->almacene->id])?>"> <?=$producto->almacene->nombre?></a></p>
      </div>
    </div>
  </div>

  <div class="col-lg-3">

      <!-- Blog Categories Well -->
      <div class="well">
          <h4>Acciones:</h4>
          <div class="row">

              <!-- /.col-lg-12 -->

              <div class="col-lg-12">
                  <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"edit",$producto->id])?>" class="btn btn-primary btn-block"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modificar</a>
                  <a href="#" onclick="if (confirm(&quot;¿Seguro que quieres eliminar este producto?&quot;)) { document.form_eliminar_almacen<?=$producto->id?>.submit(); } event.returnValue = false; return false;"class="btn btn-danger btn-block"><i class="fa fa-times" aria-hidden="true"></i> Eliminar</a>
                    <form action="<?= $this->Url->build(["controller"=>"productos","action"=>"delete", $producto->id])?>" method="post" style="display: none" name="form_eliminar_almacen<?=$producto->id?>">
                    </form>
              </div>
              <!-- /.col-lg-12 -->
          </div>
          <!-- /.row -->
      </div>

      <!-- Side Widget Well
      <div class="well">
          <h4>Información</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
      </div>-->

  </div>
</div>


<div class="col-lg-12">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4>Movimientos</h4>
          <div class="btn-group">
            <a href="<?=$this->Url->build(["controller"=>"Movimientos","action"=>"add", $producto->id, 'entrada'])?>" class="btn btn-success">Nueva Entrada</a>
            <?php if(!empty($producto->movimientos)):?>
              <a href="<?=$this->Url->build(["controller"=>"Movimientos","action"=>"add", $producto->id, 'salida'])?>" class="btn btn-warning">Nueva Salida</a>
            <?php endif;?>
          </div>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="productos">
              <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Folio</th>
                    <th>Documentos</th>
                    <th>Entrada</th>
                    <th>Salida</th>
                    <th>Existencia</th>
                    <th>Precio medio</th>
                    <th>Debe</th>
                    <th>Haber</th>
                    <th>Saldo</th>
                    <th>Acciones</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach ($producto->movimientos as $movimiento): ?>
                  <tr>
                    <td ><?= h($movimiento->fecha->format('d-m-y')) ?></td>
                    <td ><?= h($movimiento->folio) ?></td>
                    <td ><?= h($movimiento->documentos) ?></td>
                    <td ><?= $movimiento->tipo==='e'?$movimiento->cantidad:'-' ?></td>
                    <td ><?=$movimiento->tipo==='s'?$movimiento->cantidad:'-' ?></td>
                    <td><?=$movimiento->existencia?></td>
                    <td>$<?=$movimiento->precio_medio?></td>
                    <td ><?= $movimiento->tipo==='e'?'$'.$movimiento->subtotal:'-' ?></td>
                    <td ><?=$movimiento->tipo==='s'?'$'.$movimiento->subtotal:'-' ?></td>
                    <td>$<?=$movimiento->saldo?></td>
                    <td class="actions col-md-2">
                      <div class="btn-group btn-group-sm btn-group-justified">
                        <a href="<?= $this->Url->build(["controller"=>"movimientos","action"=>"edit", $movimiento->id])?>" class="btn btn-primary">Editar</a>
                        <form action="<?= $this->Url->build(["controller"=>"movimientos","action"=>"delete", $movimiento->id])?>" method="post" style="display: none" name="form_eliminar<?=$movimiento->id?>">
                        </form>
                        <a href="#" onclick="if (confirm(&quot;¿Seguro que quieres eliminar este movimiento?&quot;)) { document.form_eliminar<?=$movimiento->id?>.submit(); } event.returnValue = false; return false;" class="btn btn-danger">
                          Eliminar
                        </a>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
          </table>
          <!-- /.table-responsive -->

      </div>
      <!-- /.panel-body -->
  </div>
</div>

<?php $this->append('libraries');?>
  <?= $this->Html->script(['moment.min','datetime-moment','dataTables.buttons.min','buttons.flash.min','buttons.html5.min','jszip.min'])?>
  <?= $this->Html->css(['buttons.dataTables.min'])?>
<?php $this->end();?>

<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
    $.fn.dataTable.moment( 'DD-MM-YY' );
      $('#productos').DataTable({
        "dom": '<"col-sm-12 pull-left"fl>rt<"bottom"ip><"clear">',
        dom: 'Bfrtip',
        pageLength: 50,
        buttons: [
          {
              extend: 'excelHtml5',
              title: `Producto <?=$producto->nombre?>`,
              exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]}
          }
        ]
      });
    } );
  </script>
<?php $this->end();?>
