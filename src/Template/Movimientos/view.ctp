<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Movimiento'), ['action' => 'edit', $movimiento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Movimiento'), ['action' => 'delete', $movimiento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $movimiento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Movimientos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Movimiento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Productos'), ['controller' => 'Productos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Productos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="movimientos view large-9 medium-8 columns content">
    <h3><?= h($movimiento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Documentos') ?></th>
            <td><?= h($movimiento->documentos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <td><?= h($movimiento->tipo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Saldo') ?></th>
            <td><?= h($movimiento->saldo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Producto') ?></th>
            <td><?= $movimiento->has('producto') ? $this->Html->link($movimiento->producto->id, ['controller' => 'Productos', 'action' => 'view', $movimiento->producto->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($movimiento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cantidad') ?></th>
            <td><?= $this->Number->format($movimiento->cantidad) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Existencia') ?></th>
            <td><?= $this->Number->format($movimiento->existencia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Precio Medio') ?></th>
            <td><?= $this->Number->format($movimiento->precio_medio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subtotal') ?></th>
            <td><?= $this->Number->format($movimiento->subtotal) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($movimiento->fecha) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($movimiento->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($movimiento->modified) ?></td>
        </tr>
    </table>
</div>
