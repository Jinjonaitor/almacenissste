
<div class="col-md-9 ">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"movimientos","action"=>"add"])?>">
        <fieldset>
          <legend>Nueva Entrada</legend>
          <div class="form-group">
            <label for="fecha" class="col-lg-2 control-label">Fecha</label>
            <div class="col-lg-4">
              <input type="date" name="fecha" required="required" maxlength="255" id="fecha" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label for="folio" class="col-lg-2 control-label">Folio</label>
            <div class="col-lg-10">
              <input type="number" name="folio" required="required" maxlength="6" id="folio" class="form-control" placeholder="000000">
            </div>
          </div>
          <div class="form-group">
            <label for="documentos" class="col-lg-2 control-label">Documentos de Procedencia</label>
            <div class="col-lg-10">
              <input type="text" name="documentos" required="required" maxlength="255" id="documentos" class="form-control" placeholder="Documentos">
            </div>
          </div>
          <!-- Definir el tipo de movimiento, en este caso, Entrada = e -->
          <input type="text" name="tipo" value="e" hidden="true">
          <div class="form-group">
            <label for="cantidad" class="col-lg-2 control-label">Cantidad</label>
            <div class="col-lg-10">
              <input type="number" name="cantidad" class="form-control" id="cantidad" required="required" >
            </div>
          </div>
          <div class="form-group">
            <label for="precio_medio" class="col-lg-2 control-label">Precio Medio</label>
            <div class="col-lg-10">
              <input type="number" name="precio_medio" required="required" step=".00001" id="precio_medio" class="form-control" value="0" min=".00001">
            </div>
          </div>
          <div class="form-group">
            <label for="existencia" class="col-lg-2 control-label">Existencia</label>
            <div class="col-lg-10">
              <input type="number" name="existencia" required="required" id="existencia" class="form-control" value="<?=is_null($ultimoMovimiento)?'' :$ultimoMovimiento->existencia?>">
            </div>
          </div>
          <div class="form-group">
            <label for="subtotal" class="col-lg-2 control-label">Debe</label>
            <div class="col-lg-10">
              <input type="number" name="subtotal" required="required" step=".00001" id="subtotal" class="form-control" >
            </div>
          </div>
          <div class="form-group">
            <label for="saldo" class="col-lg-2 control-label">Saldo</label>
            <div class="col-lg-10">
              <input type="number" name="saldo" required="required" step=".00001" id="saldo" class="form-control" value="0">
            </div>
          </div>
          <input type="number" name="productos_id" value="<?=$id?>" hidden="true">
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <a href="<?= $this->Url->build(["controller"=>"productos", "action"=>"view", $id])?>"  class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>

<div class="col-md-3">
    <div class="well">
        <h4>Ultimo Movimiento:</h4>
        <?php if(is_null($ultimoMovimiento)):?>
          <div class="row">
            <div class="alert alert-danger" role="alert">Este Producto no tiene movimientos</div>
          </div>
        <?php else:?>
          <div class="form-group">
            <label>Tipo de movimiento:</label>
            <span ><?=$ultimoMovimiento->tipo==='e'?'Entrada':'Salida'?></span>
          </div>
          <div class="form-group">
            <label>Fecha del movimiento:</label>
            <span ><?=$ultimoMovimiento->fecha->format('d-m-y')?></span>
          </div>
          <div class="form-group">
            <label>Cantidad:</label>
            <span ><?=$ultimoMovimiento->cantidad?></span>
          </div>
          <div class="form-group">
            <label>Existencia:</label>
            <span ><?=$ultimoMovimiento->existencia?></span>
          </div>
          <div class="form-group">
            <label>Precio medio:</label>
            <span ><?=$ultimoMovimiento->precio_medio?></span>
          </div>
          <div class="form-group">
            <label>Saldo:</label>
            <span ><?=$ultimoMovimiento->saldo?></span>
          </div>
        <?php endif;?>
    </div>

</div>

<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#cantidad').keyup(calcular);
    $('#subtotal').keyup(calcular);
    } );

    function calcular() {
      $('#existencia').val(parseFloat(<?=is_null($ultimoMovimiento)?0 :$ultimoMovimiento->existencia?>) + parseFloat($('#cantidad').val()));
      $('#precio_medio').val( (parseFloat(<?=is_null($ultimoMovimiento)?0 :$ultimoMovimiento->saldo?>) + parseFloat($('#subtotal').val())) / parseFloat($('#existencia').val()) )
      $('#saldo').val(parseFloat($('#existencia').val()) * parseFloat($('#precio_medio').val()));
    }
  </script>
<?php $this->end();?>
