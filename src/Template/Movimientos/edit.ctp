<div class="col-md-9">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"movimientos","action"=>"edit",$movimiento->id])?>">
        <fieldset>
          <legend>Editar movimiento</legend>
          <div class="form-group">
            <label for="fecha" class="col-lg-2 control-label">Fecha</label>
            <div class="col-lg-4">
              <input type="date" name="fecha" required="required" maxlength="255" id="fecha" class="form-control" value="<?=$movimiento->fecha->format('Y-m-d')?>">
            </div>
          </div>
          <div class="form-group">
            <label for="folio" class="col-lg-2 control-label">Folio</label>
            <div class="col-lg-10">
              <input type="number" name="folio" required="required" maxlength="6" id="folio" class="form-control" value="<?=$movimiento->folio?>">
            </div>
          </div>
          <div class="form-group">
            <label for="documentos" class="col-lg-2 control-label">Documentos</label>
            <div class="col-lg-10">
              <input type="text" name="documentos" required="required" maxlength="255" id="documentos" class="form-control" value="<?=$movimiento->documentos?>">
            </div>
          </div>
          <div class="form-group">
            <label for="documentos" class="col-lg-2 control-label">Tipo de Movimiento</label>
            <div class="col-lg-10">
              <select class="form-control" name="tipo">
                <option value="e" <?php if($movimiento->tipo === 'e'){ echo'selected="true"';}?> >Entrada</option>
                <option value="s" <?php if($movimiento->tipo === 's'){ echo'selected="true"';}?> >Salida</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="cantidad" class="col-lg-2 control-label">Cantidad</label>
            <div class="col-lg-10">
              <input type="number" name="cantidad" class="form-control" id="cantidad" required="required" value="<?=$movimiento->cantidad?>">
            </div>
          </div>
          <div class="form-group">
            <label for="precio_medio" class="col-lg-2 control-label">Precio Medio</label>
            <div class="col-lg-10">
              <input type="number" name="precio_medio" required="required" step=".00001" id="precio_medio" class="form-control" value="<?=$movimiento->precio_medio?>" min=".00001">
            </div>
          </div>
          <div class="form-group">
            <label for="existencia" class="col-lg-2 control-label">Existencia</label>
            <div class="col-lg-10">
              <input type="number" name="existencia" required="required" id="existencia" class="form-control" value="<?=$movimiento->existencia?>">
            </div>
          </div>
          <div class="form-group">
            <label for="subtotal" class="col-lg-2 control-label">Debe</label>
            <div class="col-lg-10">
              <input type="number" name="subtotal" required="required" step=".00001" id="subtotal" class="form-control" value="<?=$movimiento->subtotal?>" >
            </div>
          </div>
          <div class="form-group">
            <label for="saldo" class="col-lg-2 control-label">Subtotal</label>
            <div class="col-lg-10">
              <input type="number" name="saldo" required="required" step=".00001" id="saldo" class="form-control" value="<?=$movimiento->saldo?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <a href="<?= $this->Url->build(["controller"=>"productos", "action"=>"view", $movimiento->productos_id])?>"  class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>



<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#cantidad').keyup(calcular);
    $('#precio_medio').keyup(calcular);
    } );

    function calcular() {
      $('#subtotal').val(parseFloat($('#cantidad').val()) * parseFloat($('#precio_medio').val()));
    }
  </script>
<?php $this->end();?>
