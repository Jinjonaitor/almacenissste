<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Movimientos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Productos'), ['controller' => 'Productos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Productos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="movimientos form large-9 medium-8 columns content">
    <?= $this->Form->create($movimiento) ?>
    <fieldset>
        <legend><?= __('Add Movimiento') ?></legend>
        <?php
            echo $this->Form->control('fecha');
            echo $this->Form->control('documentos');
            echo $this->Form->control('tipo');
            echo $this->Form->control('cantidad');
            echo $this->Form->control('existencia');
            echo $this->Form->control('precio_medio');
            echo $this->Form->control('subtotal');
            echo $this->Form->control('saldo');
            echo $this->Form->control('productos_id', ['options' => $productos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
