<div class="row">
  <div class="col-md-8">
      <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"movimientos","action"=>"findByFolio"])?>" enctype="multipart/form-data">
          <fieldset>
            <legend>Buscar Folios</legend>
            <div class="form-group">
              <label for="folio" class="col-lg-2 control-label">Folio</label>
              <div class="col-lg-10">
                <input type="number" name="folio" required="required" maxlength="6" id="folio" class="form-control" placeholder="000000">
              </div>
            </div>
            <div class="form-group">
              <label for="tipo" class="col-lg-2 control-label">Tipo de Movimientos</label>
              <div class="col-lg-10">
                <select class="form-control" name="tipo">
                  <option value="e">Entradas</option>
                  <option value="s">Salidas</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">Buscar</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">
        <h4>Folio <?=$folio?>, <?=$tipo==='e'?'Entradas':'Salidas'?></h4>
    </div>
      <!-- /.panel-heading -->
      <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="productos">
              <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Documentos</th>
                    <th>Articulos</th>
                    <th>Partida</th>
                    <th>Presentación</th>
                    <th>Unidad</th>
                    <th>Precio</th>
                    <th>Importe</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach ($movimientos as $movimiento): ?>
                  <tr>
                    <td ><?= h($movimiento->fecha->format('d-m-y')) ?></td>
                    <td ><?= h($movimiento->documentos) ?></td>
                    <td ><?= h($movimiento->producto->nombre) ?></td>
                    <td ><?= $movimiento->producto->partida->nombre?></td>
                    <td ><?=$movimiento->producto->presentacion?></td>
                    <td><?=$movimiento->cantidad?></td>
                    <td>$<?=$movimiento->precio_medio?></td>
                    <td >$<?=$movimiento->subtotal ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
          </table>
          <!-- /.table-responsive -->

      </div>
      <!-- /.panel-body -->
  </div>
</div>

<?php $this->append('libraries');?>
  <?= $this->Html->script(['moment.min','datetime-moment','dataTables.buttons.min','buttons.flash.min','buttons.html5.min','jszip.min'])?>
  <?= $this->Html->css(['buttons.dataTables.min'])?>
<?php $this->end();?>

<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
    $.fn.dataTable.moment( 'DD-MM-YY' );
      $('#productos').DataTable({
        "dom": '<"col-sm-12 pull-left"fl>rt<"bottom"ip><"clear">',
        dom: 'Bfrtip',
        pageLength: 50,
        buttons: [
          {
              extend: 'excelHtml5',
              title: `Folio <?=$folio?>`,
              exportOptions: {columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]}
          }
        ]
      });
    } );
  </script>
<?php $this->end();?>
