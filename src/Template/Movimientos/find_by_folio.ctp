<div class="col-md-8">
    <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" method="post" accept-charset="utf-8" action="<?= $this->Url->build(["controller"=>"movimientos","action"=>"findByFolio"])?>" enctype="multipart/form-data">
        <fieldset>
          <legend>Buscar Folios</legend>
          <div class="form-group">
            <label for="folio" class="col-lg-2 control-label">Folio</label>
            <div class="col-lg-10">
              <input type="number" name="folio" required="required" maxlength="6" id="folio" class="form-control" placeholder="000000">
            </div>
          </div>
          <div class="form-group">
            <label for="tipo" class="col-lg-2 control-label">Tipo de Movimientos</label>
            <div class="col-lg-10">
              <select class="form-control" name="tipo">
                <option value="e">Entradas</option>
                <option value="s">Salidas</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>
