<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-lg-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Descripción de la Partida</h3>
    </div>
    <div class="panel-body">
      <p>Nombre: <?=$partida->nombre?></p>
      <p>Total de productos: <?=count($partida->productos)?></p>
    </div>
  </div>
</div>

<div class="col-lg-3">

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Acciones:</h4>
        <div class="row">

            <!-- /.col-lg-12 -->

            <div class="col-lg-12">
                <a href="<?= $this->Url->build(["controller"=>"partidas","action"=>"edit",$partida->id])?>" class="btn btn-primary btn-block"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modificar</a>
                <a href="#" onclick="if (confirm(&quot;¿Seguro que quieres eliminar esta partida? Se eliminaran lso productos en esta categoría&quot;)) { document.form_eliminar_almacen<?=$partida->id?>.submit(); } event.returnValue = false; return false;"class="btn btn-danger btn-block"><i class="fa fa-times" aria-hidden="true"></i> Eliminar</a>
                  <form action="<?= $this->Url->build(["controller"=>"almacenes","action"=>"delete", $partida->id])?>" method="post" style="display: none" name="form_eliminar_almacen<?=$partida->id?>">
                  </form>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well
    <div class="well">
        <h4>Información</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>-->

</div>

<div class="col-lg-12">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4>Productos disponibles</h4>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="productos">
              <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Clave</th>
                    <th>Presentación</th>
                    <th>Existencia</th>
                    <th>Saldo</th>
                    <th>Fecha</th>
                    <th>Acciones</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach ($partida->productos as $producto): ?>
                  <tr>
                    <td ><?= h($producto->nombre) ?></td>
                    <td ><?= h($producto->clave) ?></td>
                    <td ><?= h($producto->presentacion) ?></td>
                    <td ><?=is_null($producto->saldo)? 'SM' :$producto->saldo->existencia?></td>
                    <td ><?=is_null($producto->saldo)? 'SM' :$producto->saldo->saldo?></td>
                    <td ><?=is_null($producto->saldo)? 'SM' :$producto->saldo->fecha->format('d-m-y')?></td>
                    <td class="actions col-md-3">
                      <div class="btn-group btn-group-sm btn-group-justified">
                        <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"view", $producto->id])?>" class="btn btn-default">Ver</a>
                        <a href="<?= $this->Url->build(["controller"=>"productos","action"=>"edit", $producto->id])?>" class="btn btn-primary">Editar</a>
                        <form action="<?= $this->Url->build(["controller"=>"productos","action"=>"delete", $producto->id])?>" method="post" style="display: none" name="form_eliminar<?=$producto->id?>">
                        </form>
                        <a href="#" onclick="if (confirm(&quot;¿Seguro que quieres eliminar este almacen?&quot;)) { document.form_eliminar<?=$producto->id?>.submit(); } event.returnValue = false; return false;"class="btn btn-danger">
                          Eliminar
                        </a>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
          </table>
          <!-- /.table-responsive -->

      </div>
      <!-- /.panel-body -->
  </div>
</div>

<?php $this->append('libraries');?>
  <?= $this->Html->script(['moment.min','datetime-moment','dataTables.buttons.min','buttons.flash.min','buttons.html5.min','jszip.min'])?>
  <?= $this->Html->css(['buttons.dataTables.min'])?>
<?php $this->end();?>

<?php $this->append('scripts');?>
  <script type="text/javascript">
  $(document).ready(function() {
      $('#productos').DataTable({
        "dom": '<"col-sm-12 pull-left"fl>rt<"bottom"ip><"clear">',
        dom: 'Bfrtip',
        pageLength: 50,
        buttons: [
          {
              extend: 'excelHtml5',
              title: 'Partida <?=$partida->nombre?>',
              exportOptions: {columns: [ 0, 1, 2, 3, 4, 5]}
          }
        ]
      });
    } );
  </script>
<?php $this->end();?>
