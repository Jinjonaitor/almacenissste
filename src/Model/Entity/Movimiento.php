<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Movimiento Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $fecha
 * @property string $documentos
 * @property string $tipo
 * @property int $cantidad
 * @property int $existencia
 * @property float $precio_medio
 * @property float $subtotal
 * @property string $saldo
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $productos_id
 *
 * @property \App\Model\Entity\Producto $producto
 */
class Movimiento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
