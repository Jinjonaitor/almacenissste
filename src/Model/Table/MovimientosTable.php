<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Movimientos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Productos
 *
 * @method \App\Model\Entity\Movimiento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Movimiento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Movimiento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Movimiento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Movimiento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Movimiento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Movimiento findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MovimientosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('movimientos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Productos', [
            'foreignKey' => 'productos_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('fecha')
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');

        $validator
            ->integer('folio')
            ->requirePresence('folio', 'create')
            ->notEmpty('folio');

        $validator
            ->requirePresence('documentos', 'create')
            ->notEmpty('documentos');

        $validator
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo')
            ->add('tipo', 'inList', [
                  'rule' => ['inList', ['e', 's']],
                  'message' => 'tipo no valido'
                  ]);

        $validator
            ->integer('cantidad')
            ->allowEmpty('cantidad');

        $validator
            ->integer('existencia')
            ->requirePresence('existencia', 'create')
            ->notEmpty('existencia');

        $validator
            ->decimal('precio_medio')
            ->requirePresence('precio_medio', 'create')
            ->notEmpty('precio_medio');

        $validator
            ->decimal('subtotal')
            ->requirePresence('subtotal', 'create')
            ->notEmpty('subtotal');

        $validator
            ->allowEmpty('saldo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['productos_id'], 'Productos'));

        return $rules;
    }

    public function findLastMovimiento(Query $query, array $options)
    {
        $id = $options['id'];
        return $query->where(['productos_id' => $id])->order(['created'=>'DESC'])->first();
    }
}
