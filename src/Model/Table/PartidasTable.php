<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Partidas Model
 *
 * @method \App\Model\Entity\Partida get($primaryKey, $options = [])
 * @method \App\Model\Entity\Partida newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Partida[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Partida|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Partida patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Partida[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Partida findOrCreate($search, callable $callback = null, $options = [])
 */
class PartidasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('partidas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->hasMany('Productos', [
            'foreignKey' => 'partidas_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nombre');

        return $validator;
    }
}
