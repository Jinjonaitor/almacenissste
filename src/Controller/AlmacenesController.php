<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Almacenes Controller
 *
 * @property \App\Model\Table\AlmacenesTable $Almacenes
 */
class AlmacenesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $almacenes = $this->paginate($this->Almacenes);

        $this->set(compact('almacenes'));
        $this->set('_serialize', ['almacenes']);
    }

    /**
     * View method
     *
     * @param string|null $id Almacene id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $almacen = $this->Almacenes->get($id, [
            'contain' => ['Productos.Almacenes','Productos.Partidas']
        ]);
        $this->loadModel('Movimientos');
        foreach ($almacen->productos as $producto) {
          $producto->saldo=$this->Movimientos->find('lastMovimiento', ['id'=>$producto->id]);
        }
        $this->set('almacen', $almacen);
        $this->set('_serialize', ['almacen']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $almacen = $this->Almacenes->newEntity();
        if ($this->request->is('post')) {
            $almacen = $this->Almacenes->patchEntity($almacen, $this->request->getData());
            echo($almacen);
            if ($this->Almacenes->save($almacen)) {
                $this->Flash->success(__('El almacén se ha agregado con exito.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El almacén no se pudo agregar, intentelo nuevamente.'));
        }
        $this->set(compact('almacen'));
        $this->set('_serialize', ['almacen']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Almacene id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $almacen = $this->Almacenes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $almacen = $this->Almacenes->patchEntity($almacen, $this->request->getData());
            if ($this->Almacenes->save($almacen)) {
                $this->Flash->success(__('El almacén se ha modificado con exito.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El almacén no se pudo modificar, intentelo nuevamente.'));
        }
        $this->set(compact('almacen'));
        $this->set('_serialize', ['almacen']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Almacene id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $almacen = $this->Almacenes->get($id);
        if ($this->Almacenes->delete($almacen)) {
            $this->Flash->success(__('El almacén se ha eliminado con exito.'));
        } else {
            $this->Flash->error(__('El almacén no se pudo eliminar, intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($user)
    {

        return true;
    }
}
