<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Partidas Controller
 *
 * @property \App\Model\Table\PartidasTable $Partidas
 */
class PartidasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $partidas = $this->paginate($this->Partidas);

        $this->set(compact('partidas'));
        $this->set('_serialize', ['partidas']);
    }

    /**
     * View method
     *
     * @param string|null $id Partida id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $partida = $this->Partidas->get($id,['contain'=>['Productos']]);
        $this->loadModel('Movimientos');
        foreach ($partida->productos as $producto) {
          $producto->saldo=$this->Movimientos->find('lastMovimiento', ['id'=>$producto->id]);
        }
        $this->set('partida', $partida);
        $this->set('_serialize', ['partida']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $partida = $this->Partidas->newEntity();
        if ($this->request->is('post')) {
            $partida = $this->Partidas->patchEntity($partida, $this->request->getData());
            if ($this->Partidas->save($partida)) {
                $this->Flash->success(__('The partida has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The partida could not be saved. Please, try again.'));
        }
        $this->set(compact('partida'));
        $this->set('_serialize', ['partida']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Partida id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $partida = $this->Partidas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $partida = $this->Partidas->patchEntity($partida, $this->request->getData());
            if ($this->Partidas->save($partida)) {
                $this->Flash->success(__('The partida has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The partida could not be saved. Please, try again.'));
        }
        $this->set(compact('partida'));
        $this->set('_serialize', ['partida']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Partida id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $partida = $this->Partidas->get($id);
        if ($this->Partidas->delete($partida)) {
            $this->Flash->success(__('The partida has been deleted.'));
        } else {
            $this->Flash->error(__('The partida could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($user)
    {

        return true;
    }
}
