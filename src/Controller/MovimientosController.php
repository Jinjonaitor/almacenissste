<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Movimientos Controller
 *
 * @property \App\Model\Table\MovimientosTable $Movimientos
 */
class MovimientosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Productos']
        ];
        $movimientos = $this->paginate($this->Movimientos);

        $this->set(compact('movimientos'));
        $this->set('_serialize', ['movimientos']);
    }

    /**
     * View method
     *
     * @param string|null $id Movimiento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $movimiento = $this->Movimientos->get($id, [
            'contain' => ['Productos']
        ]);

        $this->set('movimiento', $movimiento);
        $this->set('_serialize', ['movimiento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null, $tipo = null)
    {
        $movimiento = $this->Movimientos->newEntity();
        if ($this->request->is('post')) {
            $movimiento = $this->Movimientos->patchEntity($movimiento, $this->request->getData());
            if ($this->Movimientos->save($movimiento)) {
                $this->Flash->success(__('El movimiento se guardo con exito.'));

                return $this->redirect(['controller'=>'Productos','action' => 'view', $movimiento->productos_id]);
            }else{
                $this->Flash->error(__('No se pudo guardar el movimiento, por favor intenelo de nuevo'));
                return $this->redirect(['controller'=>'Productos','action' => 'view', $movimiento->productos_id]);
          }
        }
        if (is_null($tipo) || is_null($id)) {
          $this->Flash->error(__('Tipo de movimiento no seleccionado'));
          return $this->redirect($this->referer());
        }
        $ultimoMovimiento=$this->Movimientos->find('lastMovimiento', ['id'=>$id]);
        switch ($tipo) {
          case 'entrada':
            $this->set(compact('id','ultimoMovimiento'));
            return $this->render('entrada');
            break;

          case 'salida':
            $this->set(compact('id','ultimoMovimiento'));
            return $this->render('salida');
            break;

          deault:
            $this->Flash->error(__('No se pudo guardar el movimiento, por favor intenelo de nuevo'));
            return $this->redirect(['controller'=>'Productos','action' => 'view', $movimiento->productos_id]);
            break;
        }
        $productos = $this->Movimientos->Productos->find('list', ['limit' => 200]);
        $this->set(compact('movimiento', 'productos'));
        $this->set('_serialize', ['movimiento']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Movimiento id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $movimiento = $this->Movimientos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movimiento = $this->Movimientos->patchEntity($movimiento, $this->request->getData());
            if ($this->Movimientos->save($movimiento)) {
                $this->Flash->success(__('Se ha modificado el movimiento'));

                return $this->redirect(['controller'=>'Productos','action' => 'view', $movimiento->productos_id]);
            }
            $this->Flash->error(__('No se pudo modificar el movimiento'));
        }
        $productos = $this->Movimientos->Productos->find('list', ['limit' => 200]);
        $this->set(compact('movimiento', 'productos'));
        $this->set('_serialize', ['movimiento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Movimiento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $movimiento = $this->Movimientos->get($id);
        if ($this->Movimientos->delete($movimiento)) {
            $this->Flash->success(__('El movimiento se ha eliminado'));
            return $this->redirect(['controller'=>'Productos','action' => 'view', $movimiento->productos_id]);
        } else {
            $this->Flash->error(__('El movimiento no se pudo eliminar'));
            return $this->redirect(['controller'=>'Productos','action' => 'view', $movimiento->productos_id]);
        }

        return $this->redirect(['action' => 'index']);
    }


    public function findByFolio()
    {
        if ($this->request->is(['post'])) {
          $folio=$this->request->getData('folio');
          $tipo=$this->request->getData('tipo');
          $movimientos=$this->Movimientos->find('all')
              ->where(['folio'=>$folio])
              ->where(['tipo'=>$tipo])
              ->contain(['Productos','Productos.Partidas']);
          $this->set(compact('movimientos','tipo','folio'));
          return $this->render('viewByFolio');
        }

    }

    public function isAuthorized($user)
    {

        return true;
    }
}
