<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FormatosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FormatosTable Test Case
 */
class FormatosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FormatosTable
     */
    public $Formatos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.formatos',
        'app.categorias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Formatos') ? [] : ['className' => 'App\Model\Table\FormatosTable'];
        $this->Formatos = TableRegistry::get('Formatos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Formatos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
